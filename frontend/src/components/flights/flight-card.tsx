import React, { useState } from "react";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { BackendClient } from "../../clients";
import { FlightStatuses } from "../../models";

interface FlightCardProps {
  code: string;
  origin: string;
  destination: string;
  passengers?: string[];
  status: FlightStatuses;
}
const options = ["Arrived",
"Landing",
"On Time",
"Delayed",
"Cancelled"]
const mapFlightStatusToColor = (status: FlightStatuses) => {
  const mappings = {
    [FlightStatuses.Arrived]: "#1ac400",
    [FlightStatuses.Delayed]: "##c45800",
    [FlightStatuses["On Time"]]: "#1ac400",
    [FlightStatuses.Landing]: "#1ac400",
    [FlightStatuses.Cancelled]: "#ff2600",
  };

  return mappings[status] || "#000000";
};

export const FlightCard : React.FC<FlightCardProps> = (
  props: FlightCardProps
) => {
  const [selectedStatus, setStatus] = useState('');
  const handleStatusUpdate = (payload: {code: string, status: string}) => {
    const {status, code} = payload;
    // with more time, I would've used redux-toolkit and rtk-query to update and refresh list of flights accordingly. Will look at how to do this with hooks.
    const backendClient = BackendClient.getInstance();
    backendClient.updateFlight(payload);
    setStatus(status)
  }
  return  (
      <div className="card">
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <strong>{props.code}</strong>
        <Dropdown options={options} onChange={(event)=>handleStatusUpdate({code: props.code, status: event.value})} value={selectedStatus} placeholder="Select an option" />
          <span style={{ color: mapFlightStatusToColor(props.status) }}>
            Status: {props.status}
          </span>
        </div>
    
        <span>Origin: {props.origin}</span>
        <br />
        <span>Destination: {props.destination}</span>
      </div>
    );
}