import { BackendClient } from "../clients";
import { useApiCall } from "./use-api-call.hook";

const backendClient = BackendClient.getInstance();

export function useFlights() {
  return useApiCall(backendClient.getFlights);
}
// export function useFlightUpdate(payload: {code: string, status: string} | null) {
//   return useApiCall(() => backendClient.updateFlight(payload));
// }

