import express from 'express'
import { FlightsService } from '../services/flights.service'


export const flightsController = express.Router()
const flightsService = new FlightsService()

flightsController.get('/', (req, res, next) => {
    flightsService
        .getAll()
        .then((flights) => {
            res.status(200).send(flights)
        })
        .catch(next)
})
flightsController.put('/', (req, res, next) => {
    flightsService
        .updateFlight(req.body)
        .then((flights) => {
            res.status(200).send(flights)
        })
        .catch(next)
})
