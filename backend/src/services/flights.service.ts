import {FlightsModel} from '../models/flights.model'

export class FlightsService {
    async getAll() {
        try {
            const flights = await FlightsModel.find();
            return flights;
        } catch (error) {
            console.log({error})
        }
    }
    async updateFlight(payload: {code: string,status: string}) {
        try {
            const {code, status} = payload;
            const result = await FlightsModel.updateOne({code}, {status});
            return result;
        } catch (error) {
            console.log({error})
        }
    }
}
