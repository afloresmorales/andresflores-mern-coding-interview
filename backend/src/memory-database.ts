import { MongoMemoryServer } from 'mongodb-memory-server'
import mongoose from 'mongoose'
import { loadData } from '../scripts/base-data'

export const db = async ({ test = false } = {}) => {
    try {
        const mongoServer = await MongoMemoryServer.create()
        const uri = mongoServer.getUri()

        await mongoose.connect(uri)
        console.log('« Connected to In-Memory database. Loading base data... »')

        if (!test) {
            console.log('« ...Loading base data... »')
            await loadData()
            console.log('« Base data loaded to In-Memory database! »')
        }
    } catch (e) {
        console.log('ERROR', e)
    }
}
