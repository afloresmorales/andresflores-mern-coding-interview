import express from 'express'
import { flightsController } from './controllers'
import cors  from 'cors'
import { db } from './memory-database'
require('dotenv').config()

const port = process.env.PORT

const app = express()
app.use(cors())
app.use(express.json());
const v1 = express.Router()
v1.use('/flights', flightsController)

app.use('/v1', v1)

// Connect to In-Memory DB
db()

app.listen(port, () => {
    console.log(`[Live Coding Challenge] Running at http://localhost:${port}`)
})

export default app
